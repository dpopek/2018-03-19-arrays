package homework.arrays;

public class HomeworkMain {
    public static void main (String[] args){

        int[] numbersArray = new int[10];

        for (int i = 0; i < numbersArray.length; i++) {
            numbersArray[i] = i * 2 + 1;
        }
        int sum = 0;
        int product = 1;

        for (int number : numbersArray) {
            System.out.print(number + " ");
            sum = sum + number;
            product = product * number;

        }
        System.out.println();
        System.out.println("Sum = " + sum);
        System.out.println("Product = " + product);
    }
}
